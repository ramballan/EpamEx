
public class Main {

    public static void main(String[] args) {
	    XMLManager xmlMan = new XMLManager();
        xmlMan.start();
        ReportManager.printReportType1(xmlMan.getClientPersonsList(),xmlMan.getMenuItemsList());
        ReportManager.printReportType2(xmlMan.getClientPersonsList());
    }
}
