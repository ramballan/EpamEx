import objects.ClientPerson;
import objects.MenuItem;

import java.io.IOException;
import java.util.ArrayList;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * class created for work with xml files
 */
public class XMLManager {
    private ArrayList<MenuItem> menuItemsList;
    private ArrayList<ClientPerson> clientPersonsList;
    private String fileName = "input.xml";
    public void start(){
        try {
            //create DOM-analyzer
            DocumentBuilderFactory dbf=DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            //parsing document
            Document document = db.parse(fileName);
            Element root = document.getDocumentElement();

            menuItemsList = menuItemListBuilder(root);
            clientPersonsList = clientPersonListBuilder(root);
        } catch (SAXException e) {
            e.printStackTrace();
            System.out.print("SAX parser error");
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
            System.out.print("configuration error");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.print("I/O error");
        }
    }

    /**
     * This method find all MenuItems in root Element, and return them in ArrayList
     * @param root input <root></root> Element
     * @return ArrayList of MenuItem
     */
    private ArrayList<MenuItem> menuItemListBuilder(Element root) throws SAXException, IOException {
        ArrayList<MenuItem> menuItemsList = new ArrayList<MenuItem>();
        //going to menuItems location
        Element menuItemsElement = getChildElement(root,"menuItems");
        // getting List of root elements of <menuItem>
        NodeList menuItemsNodes = menuItemsElement.getElementsByTagName("menuItem");
        MenuItem menuItem = null;
        for (int i = 0; i < menuItemsNodes.getLength(); i++) {
            menuItem = new MenuItem();
            Element menuItemElement = (Element) menuItemsNodes.item(i);
            // fill MenuItem object
            menuItem.setName(getChildElementValue(menuItemElement, "name"));
            menuItem.setWeight(Float.valueOf(getChildElementValue(menuItemElement, "weight")));
            menuItem.setPrice(Float.valueOf(getChildElementValue(menuItemElement, "price")));
            menuItemsList.add(menuItem);
        }
        return menuItemsList;
    }

    /**
     * This method find all ClientPersons in root Element, and return them in ArrayList
     * @param root input <root></root> Element
     * @return ArrayList of ClientPerson
     */
    private ArrayList<ClientPerson> clientPersonListBuilder(Element root) throws SAXException, IOException {
        ArrayList<ClientPerson> clientPersonsList = new ArrayList<ClientPerson>();
        //going to clientPersons location
        Element menuItemsElement = getChildElement(root,"clientPersons");
        NodeList clientPersonNodes = menuItemsElement.getElementsByTagName("clientPerson");
        ClientPerson clientPerson = null;
        MenuItem menuItem = null;
        for (int i = 0; i < clientPersonNodes.getLength(); i++) {
            clientPerson = new ClientPerson();
            //fill ClientPerson object
            Element clientPersonElement = (Element) clientPersonNodes.item(i);
            clientPerson.setName(getChildElementValue(clientPersonElement,"name"));
            Element order = getChildElement(clientPersonElement,"order");
            //begin fill order list in ClientPerson
            NodeList dishNodes = order.getElementsByTagName("dish");
            for (int j = 0; j < dishNodes.getLength(); j++) {
                Element dishElement = (Element) dishNodes.item(j);
                //get dishname from xml
                String dishName = dishElement.getAttribute("name");
                //get menu item using name
                menuItem = getMenuItemByName(dishName);
                //if not null add menu item to order
                if (menuItem!=null) clientPerson.addDishToOrder(menuItem);
                else System.out.println("dish:"+dishName+" not in Menu");
            }
            clientPersonsList.add(clientPerson);
        }
        return clientPersonsList;
    }

    /**
     * Method return child Element
     * @param parent parent Element
     * @param childName name of child Element
     */
    private  Element getChildElement(Element parent, String childName) {
        NodeList nlist = parent.getElementsByTagName(childName);
        Element child = (Element) nlist.item(0);
        return child;
    }
    /**
     * Method getBabyValue return text, that contains in necessary Element
     * @param parent parent Element witch contains necessary Element
     * @param childName name of child Element
     * @return text, that contains in necessary Element
     */
    private  String getChildElementValue(Element parent, String childName) {
        Element child = getChildElement(parent, childName);
        Node node = child.getFirstChild();
        String value = node.getNodeValue();
        return value;
    }

    /**
     * method search by name required MenuItem in ArrayList
     * @param name dish name
     * @return MenuItem with required name
     */
    private MenuItem getMenuItemByName(String name){
        MenuItem mi = null;
        for (MenuItem menuItem : menuItemsList) {
            if (name.equals(menuItem.getName())){
                mi = menuItem;
            }
        }
        return mi;
    }

    public ArrayList<MenuItem> getMenuItemsList() {
        return menuItemsList;
    }

    public ArrayList<ClientPerson> getClientPersonsList() {
        return clientPersonsList;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }
}
