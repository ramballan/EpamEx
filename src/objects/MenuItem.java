package objects;

/**
 * All Items in menu must be MenuItem type
 */
public class MenuItem {
    private String name;
    private Float weight;
    private Float price;

    public MenuItem(String name, Float weight, Float price) {
        this.name = name;
        this.weight = weight;
        this.price = price;
    }
    public MenuItem() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return new StringBuilder(name).append(" ").append(weight).append(" ").append(price).toString();
    }
}
