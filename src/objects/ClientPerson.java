package objects;

import java.util.ArrayList;

/**
 * Class for all clients
 */
public class ClientPerson {
    private String name;
    private ArrayList<MenuItem> orderList= new ArrayList<MenuItem>();



    public ClientPerson(String name) {
        this.name = name;
    }
    public ClientPerson() {}


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<MenuItem> getOrderList() {
        return orderList;
    }

    public void setOrderList(ArrayList<MenuItem> orderList) {
        this.orderList = orderList;
    }

    public void addDishToOrder(MenuItem menuItem){
        orderList.add(menuItem);
    }
}
