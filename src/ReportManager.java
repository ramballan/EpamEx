import objects.ClientPerson;
import objects.MenuItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * creating different reports class
 */
public class ReportManager {
    /**
     * This method print to console Table with colums: MenuItem name, number of orders, price. Also print
     * the final price for all orders in last string
     * @param clientPersonsList list of Persons, who have made order
     * @param menuItemsList list of all MenuItems
     */
    public static void printReportType1(ArrayList<ClientPerson> clientPersonsList, ArrayList<MenuItem> menuItemsList){
        //key-MenuItem value-number of orders
        Map<MenuItem,Integer> map = new HashMap<MenuItem, Integer>();
        float sum = 0;
        for (ClientPerson clientPerson : clientPersonsList) {
            ArrayList<MenuItem> orderList = clientPerson.getOrderList();
            for (MenuItem menuItem : orderList) {
                Integer num = map.get(menuItem);
                //count result sum and number of each MenuItem
                map.put(menuItem, num==null ? 1 : num+1);
                sum+=menuItem.getPrice();
            }
        }
        System.out.println("\nОтчёт для столовой:");
        String separator = "+---------------------------------+------+------+";
        System.out.println(separator);
        System.out.printf("|%-32s |%6s|%6s|\n","Название блюда","Кол-во","Сумма");
        System.out.println(separator);
        for (MenuItem menuItem : menuItemsList) {
            if (map.containsKey(menuItem)){
                String dishName = menuItem.getName();
                int num = map.get(menuItem);
                float price = num*menuItem.getPrice();
                System.out.printf("|%-32s |%6d|%6.1f|\n",dishName,num,price);
                System.out.println(separator);
            }
        }
        System.out.println("Итоговая сумма: "+sum);

    }

    /**
     * This method print to console Table with colums: Person name, his order(all MenuItems names), price.
     * @param clientPersonsList list of Persons, who have made order
     */
    public static void printReportType2(ArrayList<ClientPerson> clientPersonsList){
        System.out.println("\nОтчёт для раздачи:");
        String separator = "+----------------+--------------------------------+-----+";
        System.out.println(separator);
        System.out.printf("|%-15s |%-32s|%5s|\n","Имя сотрудника","Заказанные блюда","Сумма");
        System.out.println(separator);

        for (ClientPerson clientPerson : clientPersonsList) {
            float sum = 0;
            ArrayList<MenuItem> orderList = clientPerson.getOrderList();
            for (MenuItem item : orderList) {
                sum+=item.getPrice();
            }
            for (int i = 0; i < orderList.size(); i++) {
                if (i==0) System.out.printf("|%-15s |%-32s|%5.0f|\n",clientPerson.getName(),orderList.get(i).getName(),sum);
                else System.out.printf("|%-15s |%-32s|     |\n","",orderList.get(i).getName());

            }
            System.out.println(separator);
        }
    }

}
